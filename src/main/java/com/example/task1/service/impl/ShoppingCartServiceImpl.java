package com.example.task1.service.impl;

import com.example.task1.entity.Product;
import com.example.task1.entity.ShoppingCard;
import com.example.task1.entity.dto.ProductDto;
import com.example.task1.entity.dto.ShoppingCardDto;
import com.example.task1.repo.ProductRepo;
import com.example.task1.repo.ShoppingCartRepo;
import com.example.task1.service.inter.ShoppingCartService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShoppingCartServiceImpl implements ShoppingCartService {






        private final ShoppingCartRepo shoppingCartRepository;


        private final ProductRepo productRepository;

@Override
@CachePut(cacheNames = "ShoppingCard",key = "#result.id")
        public ShoppingCardDto createShoppingCart(String name) {
            ShoppingCard cart = new ShoppingCard();
            cart.setName(name);
            ShoppingCard savedCart = shoppingCartRepository.save(cart);
            return mapToDTO(savedCart);
        }
@Override
        @Transactional
@CachePut(cacheNames = "ShoppingCard",key = "#cartId")
public ShoppingCardDto addProductToCart(Long cartId, String productId) {
    Optional<ShoppingCard> cartOpt = shoppingCartRepository.findById(cartId);
    Optional<Product> productOpt = productRepository.findById(productId);

    if (cartOpt.isPresent() && productOpt.isPresent()) {
        ShoppingCard cart = cartOpt.get();
        Product product = productOpt.get();

        cart.getProducts().add(product);

        shoppingCartRepository.save(cart);

        return mapToDTO(cart);
    }

    throw new RuntimeException("Cart or Product not found");
}
@Override
        @Transactional
@CacheEvict(cacheNames = "ShoppingCard",key = "#cartId")
public ShoppingCardDto removeProductFromCart(Long cartId, String productId) {
    Optional<ShoppingCard> cartOpt = shoppingCartRepository.findById(cartId);
    Optional<Product> productOpt = productRepository.findById(productId);

    if (cartOpt.isPresent() && productOpt.isPresent()) {
        ShoppingCard cart = cartOpt.get();
        Product product = productOpt.get();

        cart.getProducts().remove(product);

        shoppingCartRepository.save(cart);

        return mapToDTO(cart);
    }

    throw new RuntimeException("Cart or Product not found");
}
        @Override
@Cacheable(cacheNames = "ShoppingCard",key = "#cartId")
        public ShoppingCardDto getShoppingCartById(Long cartId) {
            ShoppingCard cart = shoppingCartRepository.findById(cartId)
                    .orElseThrow(() -> new RuntimeException("Cart not found"));
            return mapToDTO(cart);
        }

        private ShoppingCardDto mapToDTO(ShoppingCard cart) {
            ShoppingCardDto dto = new ShoppingCardDto();
            dto.setId(Long.valueOf(cart.getId()));
            dto.setName(cart.getName());
            dto.setProducts(cart.getProducts().stream().map(this::mapToProductDTO).collect(Collectors.toList()));
            return dto;
        }

        private ProductDto mapToProductDTO(Product product) {
            ProductDto dto = new ProductDto();
            dto.setId(Math.toIntExact(product.getId()));
            dto.setName(product.getName());
            dto.setPrice(product.getPrice());
            dto.setDescription(product.getDescription());
            dto.setCategoryName(product.getCategory() != null ? product.getCategory() : null);
            return dto;
        }
    }


