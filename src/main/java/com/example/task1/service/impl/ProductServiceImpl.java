package com.example.task1.service.impl;

import com.example.task1.entity.Category;
import com.example.task1.entity.Product;

import com.example.task1.entity.ProductDetails;
import com.example.task1.entity.dto.CategoryProductCount;
import com.example.task1.entity.dto.ProductDetailsDto;
import com.example.task1.entity.dto.ProductDto;
import com.example.task1.repo.CategoryRepository;
import com.example.task1.repo.ProductDetailsRepository;
import com.example.task1.repo.ProductRepo;
import com.example.task1.service.inter.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepo productRepository;
    private final ProductDetailsRepository productDetailsRepository;
    private final CacheManager cacheManager;
    private final CategoryRepository categoryRepository;
    @Override
    public void saveProduct(ProductDto productDto) {
        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());

        // Handle ProductDetails
        if (productDto.getProductDetails() != null) {
            ProductDetails productDetails = new ProductDetails();
            productDetails.setColor(productDto.getProductDetails().getColor());
            productDetails.setImageUrl(productDto.getProductDetails().getImageUrl());
            product.setProductDetails(productDetails);
        }

        // Handle Category
        Category category = categoryRepository.findByName(productDto.getCategoryName().getName());
        if (category == null) {
            category = new Category();
            category.setName(productDto.getCategoryName().getName());
            categoryRepository.save(category);
        }
        product.setCategory(category);

        productRepository.save(product);
        Cache cache=cacheManager.getCache("product");
        cache.put(product.getId(), product);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return productRepository.findAll().stream().map(product -> {
            ProductDto productDto = new ProductDto();
            productDto.setName(product.getName());
            productDto.setPrice(product.getPrice());
            if (product.getProductDetails() != null) {
                ProductDetailsDto productDetailsDto = new ProductDetailsDto();
                productDetailsDto.setColor(product.getProductDetails().getColor());
                productDetailsDto.setImageUrl(product.getProductDetails().getImageUrl());
                productDto.setProductDetails(productDetailsDto);
            }
            productDto.setCategoryName(product.getCategory());
            return productDto;
        }).collect(Collectors.toList());
    }

    @Override
    public List<ProductDto> getByPrice(Integer priceFrom, Integer priceTo) {

        return productRepository.findAll().stream()
                .filter(product -> product.getPrice() >= priceFrom && product.getPrice() <= priceTo)
                .map(product -> {
                    ProductDto productDto = new ProductDto();
                    productDto.setName(product.getName());
                    productDto.setPrice(product.getPrice());
                    if (product.getProductDetails() != null) {
                        ProductDetailsDto productDetailsDto = new ProductDetailsDto();
                        productDetailsDto.setColor(product.getProductDetails().getColor());
                        productDetailsDto.setImageUrl(product.getProductDetails().getImageUrl());
                        productDto.setProductDetails(productDetailsDto);
                    }
                    productDto.setCategoryName(product.getCategory());
                    return productDto;
                }).collect(Collectors.toList());
    }

    @Override
    public List<CategoryProductCount> findProductCountsByCategory() {

        return categoryRepository.findAll().stream()
                .map(category -> new CategoryProductCount(category.getName(),
                        (long) category.getProducts().size()))
                .collect(Collectors.toList());
    }
    }

