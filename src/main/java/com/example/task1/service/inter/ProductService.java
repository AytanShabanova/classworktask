package com.example.task1.service.inter;

import com.example.task1.entity.dto.CategoryProductCount;
import com.example.task1.entity.dto.ProductDto;


import java.util.List;

public interface ProductService {
  void saveProduct(ProductDto productDto);
  public List<ProductDto> getAllProducts();
  public List<ProductDto> getByPrice(Integer priceFrom, Integer priceTo);
  List<CategoryProductCount>findProductCountsByCategory();



}
