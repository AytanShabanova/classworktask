package com.example.task1.service.inter;

import com.example.task1.entity.dto.ShoppingCardDto;

public interface ShoppingCartService {
    public ShoppingCardDto createShoppingCart(String name);
    public ShoppingCardDto addProductToCart(Long cartId, String productId);
    public ShoppingCardDto removeProductFromCart(Long cartId, String productId);
    public ShoppingCardDto getShoppingCartById(Long cartId);
}
