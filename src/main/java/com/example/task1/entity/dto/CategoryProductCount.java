package com.example.task1.entity.dto;

import lombok.Data;

@Data
public class CategoryProductCount {
    private String categoryName;
    private Long productCount;

    public CategoryProductCount(String name, long size) {
    }
}
