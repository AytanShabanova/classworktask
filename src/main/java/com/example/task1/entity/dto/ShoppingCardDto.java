package com.example.task1.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class ShoppingCardDto {
    private Long id;
    private String name;
    private List<ProductDto> products;
}
