package com.example.task1.entity.dto;

import lombok.Data;

@Data
public class ProductDetailsDto {
    private String color;
    private String imageUrl;
}
