package com.example.task1.entity.dto;

import com.example.task1.entity.Category;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
   private Integer id;
    private String name;
    private Integer price;
    private Category categoryName;
    private String description;

    private ProductDetailsDto productDetails;

}
