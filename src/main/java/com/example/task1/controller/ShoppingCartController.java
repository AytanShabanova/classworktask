package com.example.task1.controller;

import com.example.task1.entity.dto.ShoppingCardDto;
import com.example.task1.service.inter.ShoppingCartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shopping-carts")
@RequiredArgsConstructor
public class ShoppingCartController {







        private final ShoppingCartService shoppingCartService;

        @PostMapping
        public ShoppingCardDto createShoppingCart(@RequestBody String name) {
            return shoppingCartService.createShoppingCart(name);
        }

        @PostMapping("/{id}/product")
        public ShoppingCardDto addProductToCart(@PathVariable Long id, @RequestBody String productId) {
            return shoppingCartService.addProductToCart(id, productId);
        }

        @DeleteMapping("/{id}/product/{productId}")
        public ShoppingCardDto removeProductFromCart(@PathVariable Long id, @PathVariable String productId) {
            return shoppingCartService.removeProductFromCart(id, productId);
        }

        @GetMapping("/{id}")
        public ShoppingCardDto getShoppingCartById(@PathVariable Long id) {
            return shoppingCartService.getShoppingCartById(id);
        }
    }


