package com.example.task1.controller;


import com.example.task1.entity.dto.CategoryProductCount;
import com.example.task1.entity.dto.ProductDto;
import com.example.task1.service.inter.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product")
public class ProductController {
    private final ProductService productService;

    @PostMapping()
    public void saveProduct(@RequestBody ProductDto productDto) {
        productService.saveProduct(productDto);
    }

    @GetMapping("/all")
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/byPrice")
    public List<ProductDto> getByPrice(@RequestParam Integer priceFrom, @RequestParam Integer priceTo) {
        return productService.getByPrice(priceFrom, priceTo);
    }

    @GetMapping("/byCategory")
    public List<CategoryProductCount> findProductCountsByCategory() {
        return productService.findProductCountsByCategory();
    }


}