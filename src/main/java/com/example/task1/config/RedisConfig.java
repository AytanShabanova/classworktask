package com.example.task1.config;

import com.example.task1.entity.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;

public class RedisConfig {
    @Value("${spring.data.redis.host}")
    private String host;
    @Value("${spring.data.redis.port}")
    private int port;

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        config.setHostName(host);
        config.setPort(port);
        return new LettuceConnectionFactory(config);


    }
    @Bean
    public RedisTemplate<Long, Product> redisTemplate() {
        RedisTemplate<Long, Product> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());
        var jacksonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        template.setKeySerializer(jacksonRedisSerializer);
        template.setValueSerializer(jacksonRedisSerializer);
        template.afterPropertiesSet();
        return template;


    }

}
