package com.example.task1.repo;

import com.example.task1.entity.ShoppingCard;
import com.example.task1.entity.dto.ShoppingCardDto;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShoppingCartRepo extends JpaRepository<ShoppingCard,Long> {


}
