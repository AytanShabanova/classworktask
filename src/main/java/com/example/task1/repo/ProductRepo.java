package com.example.task1.repo;

import com.example.task1.entity.Product;

import com.example.task1.entity.dto.CategoryProductCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product,String> {
    List<Product> findByPriceBetween(double priceFrom, double priceTo);

    @Query("SELECT p.category AS categoryName, COUNT(p) AS productCount FROM Product p GROUP BY p.category")
    List<CategoryProductCount> countProductsByCategory();
}
